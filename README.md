# CondVar rust crate #

Standard library conditional variables are subject for spurious wakeups. This crate is wrapper around std::sync::Condvar with boolean predicate that handles spurious wakeups, so you don't have to do it yourself. Based on condition_variable crate by Manuel Schölling.