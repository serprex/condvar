use std::time::{Duration, Instant};
use std::sync::{Mutex, Condvar, PoisonError, MutexGuard};

pub struct CondVar {
	pair: (Mutex<bool>, Condvar)
}

pub type CondVarResult<Guard> = Result<(), PoisonError<Guard>>;

enum Notify {
	One,
	All,
}

impl ::CondVar {
	pub fn new() -> ::CondVar {
		::CondVar {
			pair: (Mutex::new(false), Condvar::new())
		}
	}

	pub fn notify_one(&self) {
		self.notify(Notify::One)
	}

	pub fn notify_all(&self) {
		self.notify(Notify::All)
	}

	fn notify(&self, notify: Notify) {
		let &(ref lock, ref cvar) = &self.pair;

		let mut predicate = lock.lock().unwrap();
		*predicate = true;

		match notify {
			Notify::One => cvar.notify_one(),
			Notify::All => cvar.notify_all(),
		}
	}

	pub fn wait(&self) -> CondVarResult<MutexGuard<bool>> {
		let &(ref lock, ref cvar) = &self.pair;
		let mut predicate = try!(lock.lock());
		
		while !*predicate {
			predicate = try!(cvar.wait(predicate));
		}

		Ok(())
	}

	pub fn wait_timeout_ms(&self, timeout_ms: i64) -> CondVarResult<(MutexGuard<bool>,bool)> {
		if timeout_ms < 0 {
			return Ok(())
		}

		let &(ref lock, ref cvar) = &self.pair;
		let predicate = lock.lock();
		match predicate {
			Err(poison) => return Err(PoisonError::new((poison.into_inner(),false))),
			_ => ()
		}
		let mut predicate = predicate.unwrap();

		let mut remaining = Duration::from_millis(timeout_ms as u64);
		while !*predicate {
			let before = Instant::now();

			let remaining_ms = remaining.as_secs() as u32 * 1000 + remaining.subsec_nanos() / 1000_000;
			let (new, _) = try!(cvar.wait_timeout_ms(predicate, remaining_ms));
			predicate = new;

			let elapsed = before.elapsed();
			if elapsed > remaining {
				break
			}

			remaining -= elapsed;
		}

		Ok(())
	}
}

#[cfg(test)]
mod tests {
	use std::sync::Arc;
	use std::time::{Duration, Instant};
	use std::thread::{sleep, spawn};

	use ::CondVar;

	#[test]
	fn test_wait() {
		let cvar1 = Arc::new(CondVar::new());
		let cvar2 = cvar1.clone();

		spawn(move || {
			cvar2.notify_one();
		});

		cvar1.wait().unwrap();
	}

	#[test]
	fn test_wait_timeout_ms() {
		let cvar1 = Arc::new(CondVar::new());
		let cvar2 = cvar1.clone();

		spawn(move || {
			sleep(Duration::from_millis(500));
			cvar2.notify_one();
		});

		assert_eq!(cvar1.wait_timeout_ms(1000).unwrap(), ());
	}

	#[test]
	fn test_wait_timeout_ms_fail() {
		let cvar1 = Arc::new(CondVar::new());
		let cvar2 = cvar1.clone();

		spawn(move || {
			sleep(Duration::from_secs(1));
			cvar2.notify_one();
		});

		let start = Instant::now();
		cvar1.wait_timeout_ms(500).unwrap();
		if start.elapsed().as_secs() > 0 {
			panic!();
		}
	}

}

